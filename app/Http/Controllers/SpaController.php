<?php

namespace App\Http\Controllers;

use App\Models\Redirect;
use Illuminate\Support\Facades\Redirect as FacadesRedirect;

class SpaController extends Controller
{
    public function index($any = null)
    {
        $redirect = Redirect::where('redirect_from', $any)->first();
        if ($redirect) {
            $to = $redirect->to_domain . '/' . $redirect->redirect_to;
            return FacadesRedirect::to($to, 301);
        } else {
            $redirect = Redirect::where('redirect_from', '#')->where('from_domain', 'LIKE', '%' . request()->getHost() . '%')->first();
            if ($redirect) {
                $to = $redirect->to_domain . '/' . $redirect->redirect_to;
                return FacadesRedirect::to($to, 301);
            }
        }
        return FacadesRedirect::to('https://vellamerch.com/announcement', 301);
    }
}
